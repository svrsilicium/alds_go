/*
Реализуйте алгоритм, определяющий, все ли символы в строке
встречаются только один раз. А если при этом запрещено
использование дополнительных структур данных?
*/

package main

import (
	"fmt"
	"strings"
)

type uniqunes func(string) bool

// Using hash-table
func is_unique_1(s string) bool {
	ht := make(map[rune]int)

	for _, char := range strings.ToLower(s) {
		if _, ok := ht[char]; ok {
			return false
		} else {
			ht[char]++
		}
	}
	return true
}

// Using boolean vector (slice)
func is_unique_2(s string) bool {
	var chars [26]bool

	for _, char := range strings.ToLower(s) {
		if chars[char-97] {
			return false
		} else {
			chars[char-97] = true
		}
	}
	return true
}

func print(fn uniqunes, s string) {
	if fn(s) {
		fmt.Printf("%s - unique\n", s)
	} else {
		fmt.Printf("%s - not unique\n", s)
	}
}

func main() {
	print(is_unique_1, "someString")
	print(is_unique_1, "string")
	print(is_unique_2, "someString")
	print(is_unique_2, "string")
}
