/*
For two strings write a method that identifies
whether the one of strings is a permutation of the another string.
	- symbols in two permutated strings are equal but at different positions.
		how to order them?
*/

package main

import (
	"fmt"
	"strings"
)

func is_permutation(s1 string, s2 string) bool {
	/* O(n)
	 * Hash-table "rune (symbol) : int (count)" is used
	 */
	if len(s1) != len(s2) {
		return false
	}
	ht := make(map[rune]int)
	for _, char := range strings.ToLower(s1) {
		ht[char]++
	}
	for _, char := range strings.ToLower(s2) {
		ht[char]--
	}

	var sum int
	for _, count := range ht {
		sum += abs(count)
	}
	if sum > 0 {
		return false
	} else {
		return true
	}
}

func abs(x int) int {
	m := x >> 31
	return (x + m) ^ m
}

func print(str_1 string, str_2 string) {
	if is_permutation(str_1, str_2) {
		fmt.Printf("%s is premutation of %s\n", str_1, str_2)
	} else {
		fmt.Printf("%s is not premutation of %s\n", str_1, str_2)
	}
}

func main() {
	print("abcdefghar", "ghdcabeafy")
	print("abcdefgh", "ghdcabef")
	print("abcd", "ghd")
}
